#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QInputDialog>
#include <QFileDialog>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle(tr("Kochanek-Bartels Spline Editor"));
    setMouseTracking(true);
    ui->tensionLineEdit->setText("-0.7");
    ui->biasLineEdit->setText("0");
    ui->continuityLineEdit->setText("0");
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_showPointsCheckBox_toggled(bool checked)
{
    ui->widget->enablePoints(checked);
}

void MainWindow::on_showLinesCheckBox_toggled(bool checked)
{
    ui->widget->enableLines(checked);
}

void MainWindow::on_showCurvesCheckBox_toggled(bool checked)
{
    ui->widget->enableCurves(checked);
}

void MainWindow::on_removePointToolButton_clicked()
{
    ui->widget->setDeleteMode();
}

void MainWindow::on_addCurveToolButton_clicked()
{
    std::string name=QInputDialog::getText(this,tr("Input curve name"),tr("Input curve name")).toStdString();
    ui->widget->newCurve(name);
    ui->comboBox->addItem(name.c_str());
    ui->comboBox->setCurrentIndex(ui->comboBox->count()-1);
}


void MainWindow::on_biasLineEdit_textChanged(const QString &arg1)
{
    ui->widget->set_bias(arg1.toDouble());
}

void MainWindow::on_tensionLineEdit_textChanged(const QString &arg1)
{
    ui->widget->set_tension(arg1.toDouble());

}

void MainWindow::on_continuityLineEdit_textChanged(const QString &arg1)
{
    ui->widget->set_continuity(arg1.toDouble());

}

void MainWindow::on_comboBox_currentIndexChanged(const QString &arg1)
{
    ui->widget->newCurve(arg1.toStdString());
    ui->tensionLineEdit->setText(tr("%1").arg(ui->widget->get_tension()));
    ui->biasLineEdit->setText(tr("%1").arg(ui->widget->get_bias()));
    ui->continuityLineEdit->setText(tr("%1").arg(ui->widget->get_continuity()));
}


void MainWindow::on_loadPushButton_clicked()
{
    QString file=QFileDialog::getOpenFileName(this,tr("Select file to load spline"));
    if(file.size())
    {
        ui->widget->loadFromFile(file);
        std::set<std::string> s=ui->widget->getCurveNames();
        if(s.size())
        {
            ui->widget->newCurve(*s.begin());
            ui->tensionLineEdit->setText(tr("%1").arg(ui->widget->get_tension()));
            ui->biasLineEdit->setText(tr("%1").arg(ui->widget->get_bias()));
            ui->continuityLineEdit->setText(tr("%1").arg(ui->widget->get_continuity()));

        }
        ui->comboBox->clear();
        for(typeof(s.begin())i=s.begin(); i!=s.end(); i++)
        {
            ui->comboBox->addItem(i->c_str());
        }
    }

}

void MainWindow::on_savePushButton_clicked()
{
    QString _file = QFileDialog::getSaveFileName(this, tr("Select file to save spline"),
                    QString(),
                    tr("Splines (*.spline)"));
    ui->widget->saveToFile(_file);
}
