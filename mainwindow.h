#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_showPointsCheckBox_toggled(bool checked);

    void on_showLinesCheckBox_toggled(bool checked);

    void on_showCurvesCheckBox_toggled(bool checked);

    void on_removePointToolButton_clicked();

    void on_addCurveToolButton_clicked();


    void on_biasLineEdit_textChanged(const QString &arg1);

    void on_tensionLineEdit_textChanged(const QString &arg1);

    void on_continuityLineEdit_textChanged(const QString &arg1);

    void on_comboBox_currentIndexChanged(const QString &arg1);

    void on_loadPushButton_clicked();

    void on_savePushButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
