#-------------------------------------------------
#
# Project created by QtCreator 2014-09-28T13:53:04
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = unigine_test
TEMPLATE = app
INCLUDEPATH+=jsoncpp/include

SOURCES += main.cpp\
        mainwindow.cpp \
    commonError.cpp \
    glwidget.cpp \
    jsoncpp/src/lib_json/json_reader.cpp \
    jsoncpp/src/lib_json/json_value.cpp \
    jsoncpp/src/lib_json/json_writer.cpp

HEADERS  += mainwindow.h \
    commonError.h \
    compat_win32.h \
    glwidget.h \
    REF.h

FORMS    += mainwindow.ui
