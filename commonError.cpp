#include "commonError.h"
#include <stdarg.h>
#include <stdio.h>
#ifndef _WIN32
#include <syslog.h>
#endif


CommonError::CommonError(const std::string& str):m_error(str)
{
    fprintf(stderr,"CommonError raised: %s\n",m_error.c_str());
}
CommonError::CommonError(const char* fmt, ...)
{

    va_list ap;
    char str[1024*10];
    va_start(ap, fmt);
    vsnprintf(str, sizeof(str), fmt, ap);
    va_end(ap);
    m_error=str;

    fprintf(stderr,"CommonError raised: %s\n",m_error.c_str());

}
CommonError::~CommonError() throw()
{
}

void logErr2(const char* fmt, ...)
{
    {
        va_list ap,ap1;
        va_start(ap, fmt);
        va_start(ap1, fmt);
#ifndef _WIN32
        if(1)
        {
            vfprintf(stderr,fmt, ap);
            fprintf(stderr,"\n");
        }
#endif

    }
}


void assert(bool a)
{
    if(!a)
    {
        logErr2("assert failed");
        throw CommonError("assert failed");
    }
}
