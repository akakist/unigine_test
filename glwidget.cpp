
#include <QtWidgets>
#include <QtOpenGL>

#include <math.h>

#include "glwidget.h"

#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE  0x809D
#endif

GLWidget::GLWidget(QWidget *parent)
    : QGLWidget(parent),m_movingVertex(NULL)
{
    setMouseTracking(true);

    m_enabledPoints=true;
    m_enabledLines=true;
    m_enabledCurves=true;

    m_deleteMode=false;


}
GLWidget::~GLWidget()
{
}


void GLWidget::initializeGL()
{
    initializeGLFunctions();
    qglClearColor(Qt::black);

    glEnable(GL_DEPTH_TEST);

    glEnable(GL_CULL_FACE);

    glShadeModel(GL_SMOOTH);
}
void GLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    bool written=false;
    if(m_enabledPoints)
    {
        glPointSize(8);
        glBegin(GL_POINTS);
        selectColor(YELLOW);
        for(typeof(m_curves.begin()) i=m_curves.begin(); i!=m_curves.end(); i++)
        {
            REF_getter<vertex_collection> vc=i->second;
            for(typeof(vc->poligon.begin()) j=vc->poligon.begin(); j!=vc->poligon.end(); j++)
            {
                REF_getter<vertex> t=*j;
                glVertex2f(t->v.x(),t->v.y());
                written=true;
            }
        }
        glEnd();
    }
    if(m_enabledLines)
    {
        glLineWidth(1);
        for(typeof(m_curves.begin()) i=m_curves.begin(); i!=m_curves.end(); i++)
        {
            glBegin(GL_LINE_STRIP);
            selectColor(WHITE);
            REF_getter<vertex_collection> vc=i->second;
            for(typeof(vc->poligon.begin()) j=vc->poligon.begin(); j!=vc->poligon.end(); j++)
            {
                REF_getter<vertex> t=*j;
                glVertex2f(t->v.x(),t->v.y());
                written=true;

            }
            glEnd();
        }
    }

    if(m_enabledCurves)
        makeFilling(written);

    if(!written)
    {
        glBegin(GL_POINTS);
        glColor3f(0.0, 0.0, 0.0);
        glVertex2f(0,0);
        glEnd();
    }
}

void GLWidget::selectColor(int color)
{
    switch (color)
    {
    case RED:
        glColor3f(1.0, 0.0, 0.0);
        break;
    case GREEN:
        glColor3f(0.0, 1.0, 0.0);
        break;
    case BLUE:
        glColor3f(0.0, 0.0, 1.0);
        break;
    case YELLOW:
        glColor3f(1.0, 1.0, 0.0);
        break;
    case MAGENTA:
        glColor3f(1.0, 0.0, 1.0);
        break;
    case CYAN:
        glColor3f(0.0, 1.0, 1.0);
        break;
    case WHITE:
        glColor3f(1.0, 1.0, 1.0);
        break;
    }
}

void GLWidget::makeFilling(bool & written)
{
    {
        std::map<std::string,std::deque<QVector3D> > fvertex;

        for(typeof(m_curves.begin()) i0=m_curves.begin(); i0!=m_curves.end(); i0++)
        {
            std::string name=i0->first;
            REF_getter<vertex_collection> &vc=i0->second;
            size_t psz=vc->poligon.size();
            if(vc->poligon.size()>3)
            {
                for(int k=1; k<vc->poligon.size()-2; k++)
                {
                    for(int i=0; i<CURVE_SLICES; i++)
                    {
                        double u = (double)i / (double)(CURVE_SLICES-1);
                        double u2=u*u;
                        double u3=u*u*u;
                        REF_getter<vertex> v=vc->poligon[k];
                        REF_getter<vertex> vp1=vc->poligon[k+1];
                        REF_getter<vertex> vp2=vc->poligon[k+2];
                        REF_getter<vertex> vm1=vc->poligon[k-1];
                        double x= (2*u3 - 3*u2 + 1)*v->v.x()
                                  + (-2*u3 + 3*u2)*vp1->v.x()
                                  + (u3 - 2*u2 + u)*(0.5*(1-vc->tension)*((1+vc->bias)*(1-vc->continuity)*(v->v.x() - vm1->v.x())
                                                     + (1-vc->bias)*(1+vc->continuity)*(vp1->v.x() - v->v.x())))
                                  + (u3 - u2)*(0.5*(1-vc->tension)*((1+vc->bias)*(1+vc->continuity)*(vp1->v.x() - v->v.x())
                                               + (1-vc->bias)*(1-vc->continuity)*(vp2->v.x()-vp1->v.x())));

                        double y= (2*u3 - 3*u2 + 1)*v->v.y()
                                  + (-2*u3 + 3*u2)*vp1->v.y()
                                  + (u3 - 2*u2 + u)*(0.5*(1-vc->tension)*((1+vc->bias)*(1-vc->continuity)*(v->v.y()-vm1->v.y())
                                                     + (1-vc->bias)*(1+vc->continuity)*(vp1->v.y() - v->v.y())))
                                  + (u3 - u2)*(0.5*(1-vc->tension)*((1+vc->bias)*(1+vc->continuity)*(vp1->v.y()-v->v.y())
                                               + (1-vc->bias)*(1-vc->continuity)*(vp2->v.y()-vp1->v.y())));
                        double z= (2*u3 - 3*u2 + 1)*v->v.z()
                                  + (-2*u3 + 3*u2)*vp1->v.z()
                                  + (u3 - 2*u2 + u)*(0.5*(1-vc->tension)*((1+vc->bias)*(1-vc->continuity)*(v->v.z()-vm1->v.z())
                                                     + (1-vc->bias)*(1+vc->continuity)*(vp1->v.z() - v->v.z())))
                                  + (u3 - u2)*(0.5*(1-vc->tension)*((1+vc->bias)*(1+vc->continuity)*(vp1->v.z()-v->v.z())
                                               + (1-vc->bias)*(1-vc->continuity)*(vp2->v.z()-vp1->v.z())));

                        QVector3D v3=QVector3D(x,y,z);
                        fvertex[name].push_back(v3);
                    }
                }
            }

        }


        for(typeof(fvertex.begin()) i=fvertex.begin(); i!=fvertex.end(); i++)
        {
            glBegin(GL_LINE_STRIP);
            selectColor(RED);
            for (int j=0; j<i->second.size(); j++)
            {
                QVector3D& v=i->second[j];
                glVertex2f(v.x(),v.y());
                written=true;
            }
            glEnd();
        }
    }


}
void GLWidget::resizeGL(int width, int height)
{
    int side = qMin(width, height);
    glViewport(0,0,width,height);

    glLoadIdentity();
}
void GLWidget::flushCurves()
{
    std::map<std::string, REF_getter<vertex_collection> > curves;
    for(typeof(m_curves.begin()) i=m_curves.begin(); i!=m_curves.end(); i++)
    {
        REF_getter<vertex_collection>& vc=i->second;
        for(typeof(vc->poligon.begin())j=vc->poligon.begin(); j!=vc->poligon.end(); j++)
        {
            REF_getter<vertex>& t=*j;
            if(!t->erased)
            {
                if(!curves.count(i->first))
                {
                    curves.insert(std::make_pair(i->first,new vertex_collection));
                }
                curves.find(i->first)->second->poligon.push_back(t);
            }
        }
    }
    m_curves=curves;
    updateGL();
}
void GLWidget::mousePressEvent(QMouseEvent *event)
{
    if(m_curves.size()==0)
    {
        QMessageBox::warning(this,tr("Error"),tr("You need to add new curve"));
        return;
    }

    QPointF p=mouse2GL(event->pos());
    QVector3D v(p.x(),p.y(),0);

    if(m_deleteMode)
    {
        setCursor(Qt::ArrowCursor);
        m_deleteMode=false;

        REF_getter<vertex> vx=findNearestPoint(v);
        if(vx.valid()&& vx->v.distanceToPoint(v)<(20.0/double(width())))
        {
            vx->erased=true;
            flushCurves();
        }
        return;
    }
    {
        REF_getter<vertex> vx=findNearestPoint(v);
        if(vx.valid()&& vx->v.distanceToPoint(v)<(20.0/double(width())))
        {
            return;
        }

    }

    if(currentCurveName.size())
    {
        if(!m_curves.count(currentCurveName))
        {
            m_curves.insert(std::make_pair(currentCurveName, new vertex_collection));
        }
        REF_getter<vertex_collection> & v=m_curves.find(currentCurveName)->second;
        REF_getter<vertex> t=new vertex();
        QPointF glp=mouse2GL(event->pos());
        t->v.setX(glp.x());
        t->v.setY(glp.y());
        v->poligon.push_back(t);
        updateGL();

    }

}
void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton)
    {
        QPointF p=mouse2GL(event->pos());
        QVector3D v(p.x(),p.y(),0);
        if(!m_movingVertex.valid())
        {
            REF_getter<vertex> vx=findNearestPoint(v);
            if(vx.valid()&& vx->v.distanceToPoint(v)<(20.0/double(width())))
            {
                m_movingVertex=vx;
            }

        }
        if(m_movingVertex.valid())
        {
            m_movingVertex->v=v;
            updateGL();
        }
    }
    else
        m_movingVertex=NULL;

    if(event->button() &&  Qt::LeftButton)
    {
    }
}
QPointF GLWidget::mouse2GL(QPoint p)
{
    int w=width()/2;
    int h=height()/2;
    int x=p.x()-w;
    int y=height()-p.y()-h;
    return QPointF(qreal(x)/qreal(w),qreal(y)/qreal(h));
}
QPoint GLWidget::GL2mouse(QPointF p)
{
    qreal y=(1-p.y())*(height()/2)+height()/2;
    qreal x=p.x()*(width()/2)+width()/2;
    return QPoint(x,y);
}
void GLWidget::newCurve()
{
    currentCurveName=QInputDialog::getText(this,tr("Input curve name"),tr("Input curve name")).toStdString();
}

REF_getter<vertex> GLWidget::findNearestPoint(const QVector3D &p)
{
    REF_getter<vertex> nearest(NULL);
    for(typeof(m_curves.begin()) i=m_curves.begin(); i!=m_curves.end(); i++)
    {
        REF_getter<vertex_collection> &vc=i->second;
        for(typeof(vc->poligon.begin()) j=vc->poligon.begin(); j!=vc->poligon.end(); j++)
        {
            if(!nearest.valid())
                nearest=*j;
            else
            {
                REF_getter<vertex>& t=*j;
                if(t->v.distanceToPoint(p)<nearest->v.distanceToPoint(p))
                {
                    nearest=t;
                }
            }
        }
    }
    return nearest;
}
void GLWidget::enablePoints(bool v)
{
    m_enabledPoints=v;
    updateGL();
}
void GLWidget::enableLines(bool v)
{
    m_enabledLines=v;
    updateGL();
}
void GLWidget::enableCurves(bool v)
{
    m_enabledCurves=v;
    updateGL();
}

void GLWidget::set_tension(double d)
{
    if(m_curves.count(currentCurveName))
        m_curves.find(currentCurveName)->second->tension=d;
    updateGL();
}
void GLWidget::set_bias(double d)
{
    if(m_curves.count(currentCurveName))
        m_curves.find(currentCurveName)->second->bias=d;
    updateGL();
}
void GLWidget::set_continuity(double d)
{
    if(m_curves.count(currentCurveName))
        m_curves.find(currentCurveName)->second->continuity=d;
    updateGL();
}
double GLWidget::get_tension()
{
    if(!m_curves.count(currentCurveName))
        throw CommonError("if(!m_curves.count(currentCurveName))");
    return m_curves.find(currentCurveName)->second->tension;

}
double GLWidget::get_bias()
{
    if(!m_curves.count(currentCurveName))
        throw CommonError("if(!m_curves.count(currentCurveName))");
    return m_curves.find(currentCurveName)->second->bias;

}
double GLWidget::get_continuity()
{
    if(!m_curves.count(currentCurveName))
        throw CommonError("if(!m_curves.count(currentCurveName))");
    return m_curves.find(currentCurveName)->second->continuity;

}

void GLWidget::newCurve(const std::string& name)
{
    currentCurveName=name;
    if(!m_curves.count(currentCurveName))
        m_curves.insert(std::make_pair(name,new vertex_collection));
    updateGL();

}
void GLWidget::saveToFile(const QString& f)
{
    setlocale(LC_NUMERIC,"en_US.UTF-8");
    QFile file(f);
    if(!file.open(QIODevice::WriteOnly))
    {
        logErr2("cannot open %s",f.toStdString().c_str());
        return;
    }

    Json::Value v;
    for(typeof(m_curves.begin()) i=m_curves.begin(); i!=m_curves.end(); i++)
    {
        Json::Value z;
        i->second->save(z);
        v[i->first]=z;
    }
    std::string buf=v.toStyledString();
    file.write(buf.data(),buf.size());
    file.close();

}
void GLWidget::loadFromFile(const QString &f)
{
    setlocale(LC_NUMERIC,"en_US.UTF-8");
    std::string buf;
    QFile file(f);
    if(!file.open(QIODevice::ReadOnly))
    {
        return;
    }

    QByteArray arr=file.readAll();
    buf=std::string(arr.data(),arr.size());
    {
        Json::Reader rd;
        Json::Value vv;
        rd.parse(buf,vv);
        std::vector<std::string> w=vv.getMemberNames();
        for(size_t i=0; i<w.size(); i++)
        {
            Json::Value &z=vv[w[i]];
            REF_getter <vertex_collection> c=new vertex_collection;
            c->load(z);
            m_curves.insert(std::make_pair(w[i],c));
        }

    }

}
std::set<std::string> GLWidget::getCurveNames()
{
    std::set<std::string> s;
    for(typeof(m_curves.begin()) i=m_curves.begin(); i!=m_curves.end(); i++)
    {
        s.insert(i->first);
    }
    return s;
}
