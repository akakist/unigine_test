
#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QGLWidget>
#include <REF.h>
#include <deque>
#include <QGLFunctions>
#include <QVector3D>
#include "jsoncpp/include/json/json.h"
#include <set>
enum colors
{
    RED,GREEN,BLUE,YELLOW,MAGENTA,CYAN,WHITE

};
#define CURVE_SLICES 20

struct vertex: public Refcountable
{
    QVector3D v;
    bool erased;
    vertex():v(0,0,0),erased(false) {}
    void save(Json::Value& z)
    {
        z["point"]["x"]=v.x();
        z["point"]["y"]=v.y();
        z["point"]["z"]=v.z();
    }
    void load(Json::Value& z)
    {
        v=QVector3D(z["point"]["x"].asDouble(),z["point"]["y"].asDouble(),z["point"]["z"].asDouble());
    }
};
struct vertex_collection: public Refcountable
{
    vertex_collection():continuity(0),tension(-0.7),bias(0) {}
    double continuity,tension,bias;

    std::deque<REF_getter<vertex> > poligon;
    void save(Json::Value& z)
    {
        z["continuity"]=continuity;
        z["tension"]=tension;
        z["bias"]=bias;
        for(typeof(poligon.begin()) i=poligon.begin(); i!=poligon.end(); i++)
        {
            REF_getter<vertex> &t=*i;
            Json::Value v;
            t->save(v);
            z["poligon"].append(v);
        }
    }
    void load(Json::Value& z)
    {
        continuity=z["continuity"].asDouble();
        tension=z["tension"].asDouble();
        bias=z["bias"].asDouble();
        Json::Value &p=z["poligon"];
        for(size_t i=0; i<p.size(); i++)
        {
            Json::Value & el=p[i];
            REF_getter<vertex> t=new vertex;
            t->load(el);
            poligon.push_back(t);
        }
    }
};
class GLWidget : public QGLWidget, public QGLFunctions
{
    Q_OBJECT

public:
    GLWidget(QWidget *parent = 0);
    ~GLWidget();

    void enablePoints(bool v);
    void enableLines(bool v);
    void enableCurves(bool v);
    void set_tension(double d);
    void set_bias(double d);
    void set_continuity(double d);
    void newCurve(const std::string& name);
    double get_tension();
    double get_bias();
    double get_continuity();
    void saveToFile(const QString &f);
    void loadFromFile(const QString &f);

    void setDeleteMode()
    {
        m_deleteMode=true;
        setCursor(Qt::CrossCursor);
    }
    std::set<std::string> getCurveNames();
    void newCurve();


protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
private:
    std::map<std::string, REF_getter<vertex_collection> > m_curves;
    std::string currentCurveName;
    REF_getter<vertex> m_movingVertex;
    bool m_enabledPoints;
    bool m_enabledLines;
    bool m_enabledCurves;
    bool m_deleteMode;

    void makeFilling(bool &written);
    void selectColor(int color);
    QPointF mouse2GL(QPoint p);
    QPoint GL2mouse(QPointF p);
    REF_getter<vertex> findNearestPoint(const QVector3D &p);
    void flushCurves();

};

#endif
